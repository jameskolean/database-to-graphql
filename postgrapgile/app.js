var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const { postgraphile, makePluginHook } = require('postgraphile')
const PgManyToManyPlugin = require('@graphile-contrib/pg-many-to-many')
const { default: PgPubsub } = require('@graphile/pg-pubsub')

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/users', usersRouter)

const pluginHook = makePluginHook([PgPubsub])
app.use(
  postgraphile(
    process.env.DATABASE_URL || 'postgres://localhost:5432/mydb',
    'public',
    {
      appendPlugins: [PgManyToManyPlugin],
      watchPg: true,
      graphiql: true,
      enhanceGraphiql: true,
      pluginHook,
      subscriptions: true,
      simpleSubscriptions: true,
      websocketMiddlewares: [
        // Add whatever middlewares you need here, note that they should only
        // manipulate properties on req/res, they must not sent response data. e.g.:
        //
        //   require('express-session')(),
        //   require('passport').initialize(),
        //   require('passport').session(),
      ],
    }
  )
)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app

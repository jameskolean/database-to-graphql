var { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()
async function main() {
  const allUsers = await prisma.user.findMany()
  console.log(allUsers)
  const rawUsers = await prisma.raw('SELECT * FROM public."User";')
  console.log(rawUsers)
}
main()
  .catch((e) => {
    throw e
  })
  .finally(async () => {
    await prisma.disconnect()
  })
